# Cahe Library

## Overview

This library implements a memory cache that can be used to access
frequently used data stored in a database.

Data is stored as key / value pairs and keys are used to quickly lookup
the data.

Cache is limited in size and maximum number of objects that can be cached
is initialised on construction.

For performance reasons, all access methods are O(1) complexity.

## Quick Start

Simple example that

1. Initialises the cache
2. Adds data
3. Nodifies some data
4. Looks up data

```csharp
var cache = new Cache<int, string>(10);   // initialise the cache

cache.AddOrUpdate(1, "KitKat");           // add key / value pair
cache.AddOrUpdate(5, "Flowers");          // add key / value pair
cache.AddOrUpdate(11, "Cucumber");        // add key / value pair

string message;
cache.TryGetValue(1000, out message);     // message == null because key doesn't exist

cache.TryGetValue(5, out message);        // message == "Flowers"

cache.AddOrUpdate(11, "Yogurt");          // update data associated with key 11
cache.TryGetValue(11, out message);       // message == "Yogurt"
```

## Unit Testing

Unit testing is implemented in CacheLib_Tests project. I believe the test coverage is 100% of the code.
However I do not have VS 2017 Enterprise to easily verify via Code Coverage Analyzer included there.

## Design

Implementation uses `LinkedList` to store values and `Dictionary` for fast lookup by key. `key/value` pairs
are stored in the `LinkedList` to enable reverse lookup when deleting least used data.


## What Is Missing

### Cache Factory

This would allow for multiple implementations. Possibly useful for selecting fine tuned implementations.

### Performance Memory Footprint Testing

Might be a good idea to check the memory footprint behaviour. Such tests could be used as a
template for developers to confirm memory footprint of Cache<TKey, TValue> for specific
TKey, TValue the developers intend to use.
