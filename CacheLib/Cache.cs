﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheLib
{
    /// <summary>
    /// In-memory cache implementation. This can be used to avoid making expensive
    /// calls to the database.
    /// </summary>
    /// <typeparam name="TKey">Key type used for storage and access to data</typeparam>
    /// <typeparam name="TValue">Value type that stores your data</typeparam>
    public sealed class Cache<TKey, TValue> : ICache<TKey, TValue>
    {


        /// <summary>
        /// Key and Value pair.
        /// </summary>
        struct KeyValue
        {
            public TKey key;
            public TValue value;
        }

        /// <summary>
        /// Object helper used to implement thread safety.
        /// </summary>
        private object _lock = new object();

        /// <summary>
        /// Key to list Index mapping.
        /// </summary>
        private Dictionary<TKey, LinkedListNode<KeyValue>> _dictionary;

        /// <summary>
        /// Index to Value mapping. This container also stores information about
        /// least touched values to implement eviction policy.
        /// </summary>
        private LinkedList<KeyValue> _linkedList = new LinkedList<KeyValue>();

        /// <summary>
        /// Maximum number of object to be stored in the cache. 
        /// </summary>
        private int maxItemCount;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="maxItemCount">Maximum number of value objects that can be
        /// stored. Adding additional key/value pairs will result removal of the least
        /// touched value</param>
        public Cache(int maxItemCount)
        {
            if (maxItemCount == 0)
                throw new ArgumentOutOfRangeException("maxItemCount", "Zero size cache does not make sense");

            this.maxItemCount = maxItemCount;

            // set capacity on dictionary to ensure O(1) when adding new elements
            _dictionary = new Dictionary<TKey, LinkedListNode<KeyValue>>(maxItemCount);
        }

        /// <summary>
        /// Add new value or update existing value. If `key` already exists then value
        /// will be added. Otherwise existing value will be updated (replaced with supplied
        /// value).
        /// </summary>
        /// <param name="key">Key used for lookup</param>
        /// <param name="value">Value to store your data object</param>
        public void AddOrUpdate(TKey key, TValue value)
        {
            lock (_lock)
            {
                // if item already exists, update it and quit
                LinkedListNode<KeyValue> listNode;
                if (_dictionary.TryGetValue(key, out listNode))    // Dictionary<,>.TryGetValue() is O(1)
                {
                    // move item to end of list
                    _linkedList.Remove(listNode);    // LinkedList.Remove(LinkedListNode<>) is O(1)
                    _linkedList.AddLast(listNode);   // LinkedList.AddLast(LinkedListNode<>) is O(1)

                    // update value
                    listNode.Value = new KeyValue() { key = key, value = value };
                    return;
                }

                // if cache is full, remove least touched item (first in the list)
                if (maxItemCount == _linkedList.Count)
                {
                    _dictionary.Remove(_linkedList.First.Value.key);    // Dictionary<,>.Remove(key) is O(1)
                    _linkedList.RemoveFirst();                          // LinkedList.RemoveFirst() is O(1)
                }

                // add item to the end of list
                _linkedList.AddLast(new KeyValue() { key = key, value = value });   // LinkedList.AddLast(val) is O(1)
                _dictionary.Add(key, _linkedList.Last);    // Dictionary.Add(key, value) approaches O(1) if Count < Capacity
            }
        }

        /// <summary>
        /// Try get value based on the supplied key.
        /// </summary>
        /// <param name="key">Unique `key` that identifies the data to be retreived</param>
        /// <param name="value">Data object to be retreived. If `key` does not exist then
        /// value will be set to `default(TValue)`</param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (_lock)
            {
                LinkedListNode<KeyValue> listIndex;
                if (_dictionary.TryGetValue(key, out listIndex) == false)
                {
                    value = default(TValue);
                    return false;
                }

                // assign item to result and move to end of list
                _linkedList.Remove(listIndex);
                _linkedList.AddLast(listIndex);

                value = _linkedList.Last.Value.value;
                return true;
            }
        }
    }
}
