﻿using System;
using System.Diagnostics;
using CacheLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CacheLib_Tests
{
    [TestClass]
    public class Cache_Test
    {
        [TestMethod]
        public void LookUpFromEmptyCache()
        {
            var cache = new Cache<int, string>(10);

            string result;
            Assert.IsFalse(cache.TryGetValue(1, out result));
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void LookUp_SingleItem()
        {
            var cache = new Cache<int, string>(10);
            var valueCandidate = "abcd";
            cache.AddOrUpdate(1, valueCandidate);
            var storedValue = "xyz";

            Assert.AreNotEqual(valueCandidate, storedValue);
            Assert.IsTrue(cache.TryGetValue(1, out storedValue));
            Assert.AreEqual(valueCandidate, storedValue);
        }

        [TestMethod]
        public void AddTwo()
        {
            var cache = new Cache<int, string>(3);

            // fist
            cache.AddOrUpdate(1, "first");

            // second
            cache.AddOrUpdate(2, "second");

            string result;
            Assert.IsFalse(cache.TryGetValue(0, out result));
            Assert.AreEqual(null, result);

            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual("first", result);

            Assert.IsTrue(cache.TryGetValue(2, out result));
            Assert.AreEqual("second", result);

            Assert.IsFalse(cache.TryGetValue(3, out result));
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public void UpdateOne()
        {
            var cache = new Cache<int, string>(3);

            // fist
            cache.AddOrUpdate(1, "first");

            // second
            cache.AddOrUpdate(1, "first_again");

            string result;
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual("first_again", result);
        }

        [TestMethod]
        public void SimpleEvict()
        {
            var cache = new Cache<int, string>(2);

            // add fist
            cache.AddOrUpdate(1, "first");

            // add second
            cache.AddOrUpdate(2, "second");

            // touch first item, this makes the second item oldest
            string result;
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual("first", result);

            // add thrid item, this should remove second item (the oldest)
            cache.AddOrUpdate(3, "third");

            // verify second item no longer available
            Assert.IsFalse(cache.TryGetValue(2, out result));

            // verify first is still stored
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual(result, "first");

            // verify third is still stored
            Assert.IsTrue(cache.TryGetValue(3, out result));
            Assert.AreEqual(result, "third");
        }

        [TestMethod]
        public void EvictOnTen()
        {
            var cache = new Cache<int, string>(10);

            // populate with keys 0 to 9
            for (int i = 0; i < 10; ++i)
            {
                cache.AddOrUpdate(i, "i");
            }

            cache.AddOrUpdate(10, "one more");

            string result;
            Assert.IsFalse(cache.TryGetValue(0, out result));
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.IsTrue(cache.TryGetValue(9, out result));
            Assert.IsTrue(cache.TryGetValue(10, out result));
        }

        [TestMethod]
        public void EvictOnTen_WithTouchOnKeys0And1()
        {
            var cache = new Cache<int, string>(10);

            // populate with keys 0 to 9
            for (int i = 0; i < 10; ++i)
            {
                cache.AddOrUpdate(i, "i");
            }

            // touch key 0 and 1
            cache.AddOrUpdate(0, "i");
            cache.AddOrUpdate(1, "i");

            // add one more with key 10
            cache.AddOrUpdate(10, "one more");

            string result;
            Assert.IsTrue(cache.TryGetValue(0, out result));
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.IsFalse(cache.TryGetValue(2, out result));
            Assert.IsTrue(cache.TryGetValue(3, out result));
            Assert.IsTrue(cache.TryGetValue(9, out result));
            Assert.IsTrue(cache.TryGetValue(10, out result));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void NegativeCacheSize()
        {
            new Cache<int, string>(-1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ZeroSizeCache()
        {
            new Cache<int, string>(0);
        }

        [TestMethod]
        public void OneSizeCache_Add()
        {
            var cache = new Cache<int, string>(1);

            // fist
            cache.AddOrUpdate(1, "first");

            // second
            cache.AddOrUpdate(2, "second");

            string result;
            Assert.IsFalse(cache.TryGetValue(1, out result));
            Assert.AreEqual(null, result);
            Assert.IsTrue(cache.TryGetValue(2, out result));
            Assert.AreEqual("second", result);
        }

        [TestMethod]
        public void OneSizeCache_Update()
        {
            var cache = new Cache<int, string>(1);

            // fist
            cache.AddOrUpdate(1, "first");

            // second
            cache.AddOrUpdate(1, "first_replaced");

            string result;
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual("first_replaced", result);
            Assert.IsFalse(cache.TryGetValue(2, out result));
            Assert.AreEqual(null, result);
        }

        [TestMethod]
        [ExpectedException(typeof(OutOfMemoryException))]
        public void MaxCapacityCheck()
        {
            var cache = new Cache<int, string>(Int32.MaxValue);
        }

        long MeasureInserts(int firstKey, int count, Cache<int, string> cache)
        {
            Stopwatch sw = Stopwatch.StartNew();
            var max = firstKey + count;
            for (int i = firstKey; i < max; ++i)
            {
                cache.AddOrUpdate(i, "");
            }
            sw.Stop();
            return sw.ElapsedTicks;
        }

        long MeasureGet(int firstKey, int count, Cache<int, string> cache)
        {
            Stopwatch sw = Stopwatch.StartNew();
            var max = firstKey + count;
            for (int i = firstKey; i < max; ++i)
            {
                string result;
                cache.TryGetValue(i, out result);
            }
            sw.Stop();
            return sw.ElapsedTicks;
        }

        [TestMethod]
        public void Performance_Add()
        {
            Int32 cacheSize = 10000000;
            var cache = new Cache<int, string>(cacheSize);

            MeasureInserts(0, 1000, cache);

            var ticks2 = MeasureInserts(1000, 10000, cache);

            // fill up the cache to near maximum
            var eightMillion = 8000000;
            for (int i = 11000; i < eightMillion; ++i)
            {
                cache.AddOrUpdate(i, "");
            }

            var ticks3 = MeasureInserts(eightMillion, 10000, cache);

            var average = ticks2 / 2 + ticks3 / 2;
            var delta = Math.Abs(ticks2 - ticks3);
            var twentyPercentDeviation = average / 5;

            // fail if delta > 10% of measured values
            if (delta > twentyPercentDeviation)
                throw new Exception($"ticks2: {ticks2}, ticks3: {ticks3}, delta: {delta}, tenPercentDeviation: {twentyPercentDeviation}");
        }

        [TestMethod]
        public void Performance_Update()
        {
            Int32 cacheSize = 10000000;
            var cache = new Cache<int, string>(cacheSize);

            MeasureInserts(0, 1000, cache);

            MeasureInserts(1000, 10000, cache);
            var ticks2 = MeasureInserts(1000, 10000, cache);

            // fill up the cache to near maximum
            var eightMillion = 8000000;
            for (int i = 11000; i < eightMillion; ++i)
            {
                cache.AddOrUpdate(i, "");
            }

            MeasureInserts(eightMillion, 10000, cache);
            var ticks3 = MeasureInserts(eightMillion, 10000, cache);

            var average = ticks2 / 2 + ticks3 / 2;
            var delta = Math.Abs(ticks2 - ticks3);
            var twentyPercentDeviation = average / 5;

            // fail if delta > 10% of measured values
            if (delta > twentyPercentDeviation)
                throw new Exception($"ticks2: {ticks2}, ticks3: {ticks3}, delta: {delta}, tenPercentDeviation: {twentyPercentDeviation}");
        }

        [TestMethod]
        public void Performance_Get()
        {
            Int32 cacheSize = 10000000;
            var cache = new Cache<int, string>(cacheSize);

            MeasureInserts(0, 1000, cache);

            MeasureInserts(1000, 10000, cache);
            var ticks2 = MeasureGet(1000, 10000, cache);    // measure get

            // fill up the cache to near maximum
            var eightMillion = 8000000;
            for (int i = 11000; i < eightMillion; ++i)
            {
                cache.AddOrUpdate(i, "");
            }

            MeasureInserts(eightMillion, 10000, cache);
            var ticks3 = MeasureGet(eightMillion, 10000, cache);  // measure get

            var average = ticks2 / 2 + ticks3 / 2;
            var delta = Math.Abs(ticks2 - ticks3);
            var twentyPercentDeviation = average / 5;

            // fail if delta > 10% of measured values
            if (delta > twentyPercentDeviation)
                throw new Exception($"ticks2: {ticks2}, ticks3: {ticks3}, delta: {delta}, tenPercentDeviation: {twentyPercentDeviation}");
        }

        class MyNontrivialValue
        {
            public int x;
            public string y;
            public DateTime dateTime;
        }

        [TestMethod]
        public void StoreNonTrivialValue()
        {
            var cache = new Cache<int, MyNontrivialValue>(100);

            MyNontrivialValue value1 = new MyNontrivialValue() { x = 343, y = "something", dateTime = DateTime.Now };
            cache.AddOrUpdate(1, value1);

            // get invalid value
            MyNontrivialValue result;
            Assert.IsFalse(cache.TryGetValue(333, out result));
            Assert.AreEqual(null, result);

            // get valid value
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual(value1, result);

            // overwrite valid value
            MyNontrivialValue value2 = new MyNontrivialValue() { x = 111, y = "something else", dateTime = DateTime.Now };
            cache.AddOrUpdate(1, value2);

            // get modified value
            Assert.IsTrue(cache.TryGetValue(1, out result));
            Assert.AreEqual(value2, result);
        }
    }
}
